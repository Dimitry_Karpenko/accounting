@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')
    <h1 class="h2">Добавить ребенка клиента {{$client->name}} {{$client->surname}}</h1>

    <form action="/children" method="POST">

        @csrf

        @include('partials.errors')

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputCity">Имя</label>
                <input type="text" class="form-control" id="inputCity" name="name" value="{{old('name')}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputCity">Дата рождения</label>
                <input type="date" class="form-control" id="inputCity" name="birthday" value="{{old('birthday')}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputState">Пол</label>
                <select id="inputState" class="form-control" name="sex">
                    <option value="0" selected>Выбери...</option>
                    <option value="м">М</option>
                    <option value="ж">Ж</option>
                </select>
            </div>
        </div>

        <input type="hidden" name="client_id" value="{{$client->id}}">
        <button type="submit" class="btn btn-primary">Добавить</button>
        <a href="/relatives/create/{{$client->id}}" type="submit" class="btn btn-primary">Пропустить</a>
    </form>
@endsection
