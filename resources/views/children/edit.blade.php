@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')
    <h1 class="h2">Редактировать ребенка клиента {{$child->client->name}} {{$child->client->surname}}</h1>

    <form action="/children/{{$child->id}}" method="POST">

        @csrf

        @method('put')

        @include('partials.errors')

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputCity">Имя</label>
                <input type="text" class="form-control" id="inputCity" name="name" value="{{$child->name}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputCity">Дата рождения</label>
                <input type="date" class="form-control" id="inputCity" name="birthday" value="{{$child->birthday}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputState">Пол</label>
                <select id="inputState" class="form-control" name="sex">
                    <option value="0">Выбери...</option>

                    @if($child->sex == 'м')
                        <option value="м" selected>М</option>
                        <option value="ж">Ж</option>
                    @elseif($child->sex == 'ж')
                        <option value="м" >М</option>
                        <option value="ж" selected>Ж</option>
                    @endif
                </select>
            </div>
        </div>

        <input type="hidden" name="client_id" value="{{$child->client->id}}">
        <button type="submit" class="btn btn-primary">Редактировать</button>
    </form>
@endsection
