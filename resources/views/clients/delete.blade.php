@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')

    <h2 class="d-flex justify-content-center">Вы действительно хотите удалить клиента {{$client->surname}} {{$client->name}}</h2>

    <form action="/clients/{{$client->id}}" method="post">
        @csrf
        @method('delete')
        <div class="d-flex justify-content-center flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="">
                    <a href="/clients/{{$client->id}}" class="btn btn-success">Нет</a>
                    <button type="submit" class="btn btn-danger">Да</a>
                </div>
            </div>
        </div>


    </form>




@endsection
