@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')
    <h1 class="h2">Добавить нового клиента</h1>

    <form action="/clients" method="post">

        @csrf

        @include('partials.errors')

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputPassword4">Фамилия</label>
                <input type="text" class="form-control" id="inputPassword4" name="surname" value="{{old('surname')}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Имя</label>
                <input type="text" class="form-control" id="inputEmail4" name="name" value="{{old('name')}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Отчество</label>
                <input type="text" class="form-control" id="inputEmail4" name="otchestvo" value="{{old('otchestvo')}}">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Телефон</label>
                <input type="text" class="form-control" id="inputEmail4" name="tel" value="{{old('tel')}}">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Вайбер</label>
                <input type="text" class="form-control" id="inputPassword4" value="+38" name="viber">
            </div>
        </div>
        <div class="form-group">
            <label for="inputAddress">Адрес</label>
            <input type="text" class="form-control" id="inputAddress" name="address" value="{{old('address')}}">
        </div>

        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
@endsection
