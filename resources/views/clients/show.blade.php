@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')
    {{--   <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4"><div style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;" class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>--}}
    <div class="d-flex justify-content-end flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">

        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="">
                <a href="/clients/{{$client->id}}/edit" class="btn btn-success">Редактировать</a>
                <a href="/clients/{{$client->id}}/delete" class="btn btn-danger">Удалить</a>
            </div>
        </div>
    </div>

    <h3>Клиент</h3>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>ID</th>
                <th>ФИО</th>
                <th>Тел</th>
                <th>Viber</th>
                <th>Адрес</th>
            </tr>
            </thead>
            <tbody>


                <tr>
                    <td>{{$client->id}}</td>
                    <td>{{$client->surname}} {{$client->name}} {{$client->otchestvo}}</td>
                    <td><a href="tel:{{$client->tel}}">{{$client->tel}}</a></td>
                    <td><a href="viber://chat?number={{$client->viber}}">{{$client->viber}}</a></td>
                    <td>{{$client->address}}</td>
                </tr>


            </tbody>
        </table>

        @if(!empty($client->relatives[0]))

            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>Родственник</th>
                    <th>Род.связь</th>
                    <th>Тел</th>
                    <th>Рд</th>
                    <th>Уд</th>
                </tr>
                </thead>
                <tbody>


                @foreach($client->relatives as $key => $relative)


                    <tr>
                        <td>{{$relative->name}}</td>
                        <td>{{$relative->relationship}}</td>
                        <td><a href="tel:{{$relative->tel}}">{{$relative->tel}}</a></td>
                        <td><a href="/relatives/{{$relative->id}}/edit"><svg id="_x31_" enable-background="new 0 0 24 24" height="20px" viewBox="0 0 24 24" width="20px" xmlns="http://www.w3.org/2000/svg"><path d="m11.894 24c-.131 0-.259-.052-.354-.146-.118-.118-.17-.288-.137-.451l.707-3.535c.02-.098.066-.187.137-.256l7.778-7.778c.584-.584 1.537-.584 2.121 0l1.414 1.414c.585.585.585 1.536 0 2.121l-7.778 7.778c-.069.07-.158.117-.256.137l-3.535.707c-.032.006-.065.009-.097.009zm1.168-3.789-.53 2.651 2.651-.53 7.671-7.671c.195-.195.195-.512 0-.707l-1.414-1.414c-.195-.195-.512-.195-.707 0zm2.367 2.582h.01z"/><path d="m9.5 21h-7c-1.379 0-2.5-1.121-2.5-2.5v-13c0-1.379 1.121-2.5 2.5-2.5h2c.276 0 .5.224.5.5s-.224.5-.5.5h-2c-.827 0-1.5.673-1.5 1.5v13c0 .827.673 1.5 1.5 1.5h7c.276 0 .5.224.5.5s-.224.5-.5.5z"/><path d="m16.5 12c-.276 0-.5-.224-.5-.5v-6c0-.827-.673-1.5-1.5-1.5h-2c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h2c1.379 0 2.5 1.121 2.5 2.5v6c0 .276-.224.5-.5.5z"/><path d="m11.5 6h-6c-.827 0-1.5-.673-1.5-1.5v-2c0-.276.224-.5.5-.5h1.55c.232-1.14 1.243-2 2.45-2s2.218.86 2.45 2h1.55c.276 0 .5.224.5.5v2c0 .827-.673 1.5-1.5 1.5zm-6.5-3v1.5c0 .275.225.5.5.5h6c.275 0 .5-.225.5-.5v-1.5h-1.5c-.276 0-.5-.224-.5-.5 0-.827-.673-1.5-1.5-1.5s-1.5.673-1.5 1.5c0 .276-.224.5-.5.5z"/><path d="m13.5 9h-10c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h10c.276 0 .5.224.5.5s-.224.5-.5.5z"/><path d="m13.5 12h-10c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h10c.276 0 .5.224.5.5s-.224.5-.5.5z"/><path d="m13.5 15h-10c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h10c.276 0 .5.224.5.5s-.224.5-.5.5z"/></svg></a></td>
                        <td><a href="/relatives/{{$relative->id}}/delete"><svg class="svg-delete" height="20px" viewBox="-57 0 512 512" width="20px" xmlns="http://www.w3.org/2000/svg"><path d="m156.371094 30.90625h85.570312v14.398438h30.902344v-16.414063c.003906-15.929687-12.949219-28.890625-28.871094-28.890625h-89.632812c-15.921875 0-28.875 12.960938-28.875 28.890625v16.414063h30.90625zm0 0"/><path d="m344.210938 167.75h-290.109376c-7.949218 0-14.207031 6.78125-13.566406 14.707031l24.253906 299.90625c1.351563 16.742188 15.316407 29.636719 32.09375 29.636719h204.542969c16.777344 0 30.742188-12.894531 32.09375-29.640625l24.253907-299.902344c.644531-7.925781-5.613282-14.707031-13.5625-14.707031zm-219.863282 312.261719c-.324218.019531-.648437.03125-.96875.03125-8.101562 0-14.902344-6.308594-15.40625-14.503907l-15.199218-246.207031c-.523438-8.519531 5.957031-15.851562 14.472656-16.375 8.488281-.515625 15.851562 5.949219 16.375 14.472657l15.195312 246.207031c.527344 8.519531-5.953125 15.847656-14.46875 16.375zm90.433594-15.421875c0 8.53125-6.917969 15.449218-15.453125 15.449218s-15.453125-6.917968-15.453125-15.449218v-246.210938c0-8.535156 6.917969-15.453125 15.453125-15.453125 8.53125 0 15.453125 6.917969 15.453125 15.453125zm90.757812-245.300782-14.511718 246.207032c-.480469 8.210937-7.292969 14.542968-15.410156 14.542968-.304688 0-.613282-.007812-.921876-.023437-8.519531-.503906-15.019531-7.816406-14.515624-16.335937l14.507812-246.210938c.5-8.519531 7.789062-15.019531 16.332031-14.515625 8.519531.5 15.019531 7.816406 14.519531 16.335937zm0 0"/><path d="m397.648438 120.0625-10.148438-30.421875c-2.675781-8.019531-10.183594-13.429687-18.640625-13.429687h-339.410156c-8.453125 0-15.964844 5.410156-18.636719 13.429687l-10.148438 30.421875c-1.957031 5.867188.589844 11.851562 5.34375 14.835938 1.9375 1.214843 4.230469 1.945312 6.75 1.945312h372.796876c2.519531 0 4.816406-.730469 6.75-1.949219 4.753906-2.984375 7.300781-8.96875 5.34375-14.832031zm0 0"/></svg></a></td>
                    </tr>

                @endforeach

                @endif


        @if(!empty($client->children[0]))

            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>Ребенок</th>
                    <th>Пол</th>
                    <th>Возраст</th>
                    <th>Др</th>
                    <th>Рд</th>
                    <th>Уд</th>
                </tr>
                </thead>
                <tbody>


        @foreach($client->children as $key => $child)


                    <tr>
                        <td>{{$child->name}}</td>
                        <td>{{$child->sex}}</a></td>
                        <td>{{$child->getAge()}}</td>
                        <td>{{$child->getBirthday()}}</td>
                        <td><a href="/children/{{$child->id}}/edit"><svg id="_x31_" enable-background="new 0 0 24 24" height="20px" viewBox="0 0 24 24" width="20px" xmlns="http://www.w3.org/2000/svg"><path d="m11.894 24c-.131 0-.259-.052-.354-.146-.118-.118-.17-.288-.137-.451l.707-3.535c.02-.098.066-.187.137-.256l7.778-7.778c.584-.584 1.537-.584 2.121 0l1.414 1.414c.585.585.585 1.536 0 2.121l-7.778 7.778c-.069.07-.158.117-.256.137l-3.535.707c-.032.006-.065.009-.097.009zm1.168-3.789-.53 2.651 2.651-.53 7.671-7.671c.195-.195.195-.512 0-.707l-1.414-1.414c-.195-.195-.512-.195-.707 0zm2.367 2.582h.01z"/><path d="m9.5 21h-7c-1.379 0-2.5-1.121-2.5-2.5v-13c0-1.379 1.121-2.5 2.5-2.5h2c.276 0 .5.224.5.5s-.224.5-.5.5h-2c-.827 0-1.5.673-1.5 1.5v13c0 .827.673 1.5 1.5 1.5h7c.276 0 .5.224.5.5s-.224.5-.5.5z"/><path d="m16.5 12c-.276 0-.5-.224-.5-.5v-6c0-.827-.673-1.5-1.5-1.5h-2c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h2c1.379 0 2.5 1.121 2.5 2.5v6c0 .276-.224.5-.5.5z"/><path d="m11.5 6h-6c-.827 0-1.5-.673-1.5-1.5v-2c0-.276.224-.5.5-.5h1.55c.232-1.14 1.243-2 2.45-2s2.218.86 2.45 2h1.55c.276 0 .5.224.5.5v2c0 .827-.673 1.5-1.5 1.5zm-6.5-3v1.5c0 .275.225.5.5.5h6c.275 0 .5-.225.5-.5v-1.5h-1.5c-.276 0-.5-.224-.5-.5 0-.827-.673-1.5-1.5-1.5s-1.5.673-1.5 1.5c0 .276-.224.5-.5.5z"/><path d="m13.5 9h-10c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h10c.276 0 .5.224.5.5s-.224.5-.5.5z"/><path d="m13.5 12h-10c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h10c.276 0 .5.224.5.5s-.224.5-.5.5z"/><path d="m13.5 15h-10c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h10c.276 0 .5.224.5.5s-.224.5-.5.5z"/></svg></a></td>
                        <td><a href="/children/{{$child->id}}/delete"><svg class="svg-delete" height="20px" viewBox="-57 0 512 512" width="20px" xmlns="http://www.w3.org/2000/svg"><path d="m156.371094 30.90625h85.570312v14.398438h30.902344v-16.414063c.003906-15.929687-12.949219-28.890625-28.871094-28.890625h-89.632812c-15.921875 0-28.875 12.960938-28.875 28.890625v16.414063h30.90625zm0 0"/><path d="m344.210938 167.75h-290.109376c-7.949218 0-14.207031 6.78125-13.566406 14.707031l24.253906 299.90625c1.351563 16.742188 15.316407 29.636719 32.09375 29.636719h204.542969c16.777344 0 30.742188-12.894531 32.09375-29.640625l24.253907-299.902344c.644531-7.925781-5.613282-14.707031-13.5625-14.707031zm-219.863282 312.261719c-.324218.019531-.648437.03125-.96875.03125-8.101562 0-14.902344-6.308594-15.40625-14.503907l-15.199218-246.207031c-.523438-8.519531 5.957031-15.851562 14.472656-16.375 8.488281-.515625 15.851562 5.949219 16.375 14.472657l15.195312 246.207031c.527344 8.519531-5.953125 15.847656-14.46875 16.375zm90.433594-15.421875c0 8.53125-6.917969 15.449218-15.453125 15.449218s-15.453125-6.917968-15.453125-15.449218v-246.210938c0-8.535156 6.917969-15.453125 15.453125-15.453125 8.53125 0 15.453125 6.917969 15.453125 15.453125zm90.757812-245.300782-14.511718 246.207032c-.480469 8.210937-7.292969 14.542968-15.410156 14.542968-.304688 0-.613282-.007812-.921876-.023437-8.519531-.503906-15.019531-7.816406-14.515624-16.335937l14.507812-246.210938c.5-8.519531 7.789062-15.019531 16.332031-14.515625 8.519531.5 15.019531 7.816406 14.519531 16.335937zm0 0"/><path d="m397.648438 120.0625-10.148438-30.421875c-2.675781-8.019531-10.183594-13.429687-18.640625-13.429687h-339.410156c-8.453125 0-15.964844 5.410156-18.636719 13.429687l-10.148438 30.421875c-1.957031 5.867188.589844 11.851562 5.34375 14.835938 1.9375 1.214843 4.230469 1.945312 6.75 1.945312h372.796876c2.519531 0 4.816406-.730469 6.75-1.949219 4.753906-2.984375 7.300781-8.96875 5.34375-14.832031zm0 0"/></svg></a></td>
                    </tr>

        @endforeach

        @endif
                </tbody>
            </table>



        <div class="d-flex justify-content-center flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div class="btn-group" role="group" aria-label="Basic example">
                <a href="/children/create/{{$client->id}}" type="button" class="btn btn-outline-primary">Добавить ребенка</a>
                <a href="/relatives/create/{{$client->id}}" type="button" class="btn btn-outline-warning">Добавить родственника</a>
                <a href="/clients/{{$client->id}}/category" type="button" class="btn btn-outline-success">Добавить заказ</a>
            </div>
        </div>


    </div>

    @if(!empty($client->orders[0]))

        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>Товар</th>
                <th>Залог</th>
                <th>Тариф</th>
                <th>Cкидка</th>
                <th>Итого</th>
                <th>По</th>
            </tr>
            </thead>
            <tbody>


            @foreach($client->orders as $order)
                @if($order->finishedTerm())
                    <tr style="background-color: orangered">
                @elseif($order->finishedTerm('+4days'))
                    <tr style="background-color: orange">
                @elseif($order->active)
                    <tr style="background-color: aqua">
                @else
                    <tr>
                @endif
                    <td><a href="/orders/{{$order->id}}">{{$order->product_type}} - {{$order->product_name}}</a></td>
                    <td>{{$order->deposit}}</a></td>
                    <td>{{$order->currentTarif()}} за {{$order->currentPeriodInHuman()}}</td>
                    <td>
                        @if($order->currentDiscount())
                            {{$order->currentDiscount()}}
                        @else
                            нет
                        @endif
                    </td>
                    <td>{{$order->currentSum()}}</td>
                    <td>{{$order->getFinishDate()}}</td>
                </tr>
                @if($order->comment)
                    @if($order->finishedTerm())
                        <tr style="background-color: orangered">
                    @elseif($order->finishedTerm('+4days'))
                        <tr style="background-color: orange">
                    @elseif($order->active)
                        <tr style="background-color: aqua">
                    @else
                        <tr>
                    @endif
                        <td colspan="6">
                            Коммент.: {{$order->comment}}
                        </td>
                    </tr>
                @endif
                <tr>
                    <td colspan="6">
                        <div class="d-flex justify-content-center flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a href="/periods/create/{{$order->id}}" type="button" class="btn btn-outline-primary">Продлить</a>
                                <a href="/relatives/create/{{$client->id}}" type="button" class="btn btn-outline-warning">Оплатить</a>
                                <a href="/clients/{{$client->id}}/category" type="button" class="btn btn-outline-success">Возврат</a>
                            </div>
                        </div>
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>

    @endif

@endsection
