@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')

    <h3>Просроченые заказы</h3>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>Клиент</th>
                <th>Тел</th>
                <th>Товар</th>
                <th>Залог</th>
                <th>Тариф</th>
                <th>По</th>
            </tr>
            </thead>
            <tbody>

            @foreach($orders as $order)
                @if($order->finishedTerm())
                    <tr style="background-color: orangered">
                        <td>{{$order->client->name}} {{$order->client->surname}}</td>
                        <td>{{$order->client->tel}}</td>
                        <td>{{$order->product_type}} - {{$order->product_name}}</td>
                        <td>Залог {{$order->deposit}}</td>
                        <td>{{$order->tarif}} за {{$order->periodInHuman()}}</td>
                        <td>{{$order->getFinishDate()}}</td>
                    </tr>
                @endif
            @endforeach


            </tbody>
        </table>


    </div>
@endsection
