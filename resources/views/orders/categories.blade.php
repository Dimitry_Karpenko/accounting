@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')

    <h3>Клиент</h3>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>ФИО</th>
                <th>Тел</th>
                <th>Viber</th>
                <th>Адрес</th>
            </tr>
            </thead>
            <tbody>


            <tr>
                <td>{{$client->id}}</td>
                <td>{{$client->surname}} {{$client->name}}</td>
                <td><a href="tel:{{$client->tel}}">{{$client->tel}}</a></td>
                <td><a href="viber://chat?number={{$client->viber}}">viber</a></td>
                <td>{{$client->address}}</td>
            </tr>


            </tbody>
        </table>


    </div>
    <h2>
        Выберите категорию товаров
        <a href="/clients/{{$client->id}}" type="button" class="btn btn-outline-primary">Назад</a>

    </h2>
        @foreach($categories as $category)
                <a class="btn btn-primary" href="/clients/{{$client->id}}/category/{{$category->id}}">{{$category->name}}</a>
        @endforeach

@endsection
