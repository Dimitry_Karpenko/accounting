@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')
    {{--   <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4"><div style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;" class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>--}}
    <div class="d-flex justify-content-end flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">

        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="">
                <a href="/orders/{{$order->id}}/edit" class="btn btn-success">Редактировать</a>
                <a href="/orders/{{$order->id}}/delete" class="btn btn-danger">Удалить</a>
            </div>
        </div>
    </div>

    <h3>Заказ клиента {{$order->client->surname}}</h3>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>Товар</th>
                <th>Залог</th>
                <th>Дата создания</th>
                <th>Добавил</th>
            </tr>
            </thead>
            <tbody>


            <tr>
                <td>{{$order->product_name}} {{$order->product_type}}</td>
                <td>{{$order->deposit}}</td>
                <td>{{date('d-m-Y', strtotime($order->created_at))}}</td>
                <td>{{$order->user->name}}</td>
            </tr>


            </tbody>
        </table>
    </div>

    @if($order->comment)
        <p>Комментарий: {{$order->comment}}</p>
    @endif

@endsection
