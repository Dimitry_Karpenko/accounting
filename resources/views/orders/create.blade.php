@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')
    <h3>Клиент</h3>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>ФИО</th>
                <th>Тел</th>
                <th>Viber</th>
                <th>Адрес</th>
            </tr>
            </thead>
            <tbody>


            <tr>
                <td>{{$client->id}}</td>
                <td>{{$client->surname}} {{$client->name}}</td>
                <td><a href="tel:{{$client->tel}}">{{$client->tel}}</a></td>
                <td><a href="viber://chat?number={{$client->viber}}">viber</a></td>
                <td>{{$client->address}}</td>
            </tr>


            </tbody>
        </table>

    </div>
    <h2>
        Оформить заказ
        <a href="/clients/{{$client->id}}/category/{{$product->category->id}}" type="button" class="btn btn-outline-primary">Назад</a>
    </h2>

    <form action="/orders" method="post">

        @csrf

        @include('partials.errors')

        <input type="hidden" name="client_id" value="{{$client->id}}">
        <input type="hidden" name="product_id" value="{{$product->id}}">

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Название</label>
                <input type="text" class="form-control" id="inputEmail4" name="product_name" value="{{$product->name}}" readonly>
            </div>

            <div class="form-group col-md-6">
                <label for="inputState">Категория</label>
                <input type="text" class="form-control" id="inputEmail4" name="product_type" value="{{$product->category->name}}" readonly>
            </div>

            <div class="form-group col-md-3">
                <label for="inputState">С (дата начала)</label>
                <input type="date" class="form-control" id="inputEmail4" name="date_start" value="{{date('Y-m-d')}}">
            </div>

            <div class="form-group col-md-3">
                <label for="inputEmail4">Залог</label>
                <input type="number" class="form-control" id="inputEmail4" name="deposit" value="{{$product->deposit}}">
            </div>

            <div class="form-group col-md-3">
                <label for="inputEmail4">Тариф</label>
                <select type="number" min="0" class="form-control" id="inputEmail4" name="period">
                    @if($product->m3_tarif > 0)
                        <option value="3months">{{$product->m3_tarif}} грн./3мес</option>
                    @endif
                    @if($product->m2_tarif > 0)
                        <option value="2months">{{$product->m2_tarif}} грн./2мес</option>
                    @endif
                    @if($product->m1_tarif > 0)
                        <option value="1month">{{$product->m1_tarif}} грн./1мес</option>
                    @endif
                    @if($product->w3_tarif > 0)
                        <option value="3weeks">{{$product->w3_tarif}} грн./3нед</option>
                    @endif
                    @if($product->w2_tarif > 0)
                        <option value="2weeks">{{$product->w2_tarif}} грн./2нед</option>
                    @endif
                    @if($product->w1_tarif > 0)
                        <option value="1week">{{$product->w1_tarif}} грн./1нед</option>
                    @endif
                </select>
            </div>

            <div class="form-group col-md-3">
                <label for="inputEmail4">Cкидка</label>
                <input type="number" class="form-control" id="inputEmail4" name="discount" placeholder="Скидка в грн.">
            </div>

            <div class="form-group col-md-12">
                <label for="inputEmail4">Комментарий</label>
                <input type="text" class="form-control" id="inputEmail4" name="comment">
            </div>

        <button type="submit" class="btn btn-primary">Оформить</button>
    </form>

@endsection
