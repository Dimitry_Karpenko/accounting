@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')
   <h2>Редактировать заказ {{$order->client->surname}} {{$order->client->name}}</h2>

    <form action="/orders/{{$order->id}}" method="post">

        @csrf
        @method('put')

        @include('partials.errors')

        <input type="hidden" name="client_id" value="{{$order->client_id}}">
        <input type="hidden" name="product_id" value="{{$order->product_id}}">

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputEmail4">Название</label>
                <input type="text" class="form-control" id="inputEmail4" name="product_name" value="{{$order->product_name}}">
            </div>

            <div class="form-group col-md-4">
                <label for="inputState">Категория</label>
                <input type="text" class="form-control" id="inputEmail4" name="product_type" value="{{$order->product_type}}">
            </div>

            <div class="form-group col-md-4">
                <label for="inputEmail4">Залог</label>
                <input type="number" class="form-control" id="inputEmail4" name="deposit" value="{{$order->deposit}}">
            </div>

            <div class="form-group col-md-12">
                <label for="inputEmail4">Комментарий</label>
                <input type="text" class="form-control" id="inputEmail4" name="comment" value="{{$order->comment}}">
            </div>

            <button type="submit" class="btn btn-primary">Редактировать</button>
    </form>

@endsection
