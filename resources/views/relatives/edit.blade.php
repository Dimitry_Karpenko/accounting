@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')
    <h1 class="h2">Редактировать родственника клиента {{$relative->client->name}}</h1>

    <form action="/relatives/{{$relative->id}}" method="post">

        @csrf
        @method('put')

        @include('partials.errors')

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputEmail4">Имя</label>
                <input type="text" class="form-control" id="inputEmail4" name="name" value="{{$relative->name}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Род.связь</label>
                <input type="text" class="form-control" id="inputEmail4" name="relationship" value="{{$relative->relationship}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Телефон</label>
                <input type="text" class="form-control" id="inputEmail4" name="tel" value="{{$relative->tel}}">
            </div>
        </div>

        <input type="hidden" name="client_id" value="{{$relative->client_id}}">

        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
@endsection
