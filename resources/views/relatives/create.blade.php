@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')
    <h1 class="h2">Добавить родственника клиента {{$client->surname}}</h1>

    <form action="/relatives" method="post">

        @csrf

        @include('partials.errors')

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputEmail4">Имя</label>
                <input type="text" class="form-control" id="inputEmail4" name="name" value="{{old('name')}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Род.связь</label>
                <input type="text" class="form-control" id="inputEmail4" name="relationship" value="{{old('relationship')}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Телефон</label>
                <input type="text" class="form-control" id="inputEmail4" name="tel" value="{{old('tel')}}">
            </div>
        </div>

        <input type="hidden" name="client_id" value="{{$client->id}}">

        <button type="submit" class="btn btn-primary">Сохранить</button>
        <a href="/clients/{{$client->id}}" type="submit" class="btn btn-primary">Пропустить</a>
    </form>
@endsection
