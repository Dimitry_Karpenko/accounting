@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')

    <h1 class="h2">Добавить новую категорию</h1>

    <form action="/categories/{{$category->id}}" method="POST">

        @csrf

        @method('put')

        @include('partials.errors')

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputCity">Название</label>
                <input type="text" class="form-control" id="inputCity" name="name" value="{{$category->name}}">
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Редактировать</button>
    </form>
@endsection
