@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-product" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск товара" aria-label="Search" name="query">
    </form>
@endsection

@section('content')
    <h1 class="h2">Редактировать</h1>

    <form action="/products/{{$product->id}}" method="post">

        @csrf

        @method('put')

        @include('partials.errors')

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputEmail4">Название</label>
                <input type="text" class="form-control" id="inputEmail4" name="name" value="{{$product->name}}">
            </div>

            <div class="form-group col-md-4">
                <label for="inputState">Категория</label>
                <select name="category_id" id="inputState" class="form-control">
                    <option selected>Выбери...</option>
                    @foreach($categories as $category)

                        @if($product->category_id == $category->id)
                            <option value="{{$category->id}}" selected>{{$category->name}}</option>
                        @else
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endif

                    @endforeach
                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="inputEmail4">Залог</label>
                <input type="number" min="0" class="form-control" id="inputEmail4" name="deposit" value="{{$product->deposit}}">
            </div>

            <div class="form-group col-md-4">
                <label for="inputEmail4">Тариф 1 мес</label>
                <input type="number" min="0" class="form-control" id="inputEmail4" name="m1_tarif" value="{{$product->m1_tarif}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Тариф 2 мес</label>
                <input type="number" min="0" class="form-control" id="inputEmail4" name="m2_tarif" value="{{$product->m2_tarif}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Тариф 3 мес</label>
                <input type="number" min="0" class="form-control" id="inputEmail4" name="m3_tarif" value="{{$product->m3_tarif}}">
            </div>

            <div class="form-group col-md-4">
                <label for="inputEmail4">Тариф 1 нед</label>
                <input type="number" min="0" class="form-control" id="inputEmail4" name="w1_tarif" value="{{$product->w1_tarif}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Тариф 2 нед</label>
                <input type="number" min="0" class="form-control" id="inputEmail4" name="w2_tarif" value="{{$product->w2_tarif}}">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Тариф 3 нед</label>
                <input type="number" min="0" class="form-control" id="inputEmail4" name="w3_tarif" value="{{$product->w3_tarif}}">
            </div>

        </div>

        <button type="submit" class="btn btn-primary">Редкатировать</button>
    </form>
@endsection
