@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-product" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск товара" aria-label="Search" name="query">
    </form>
@endsection

@section('content')
    <h1 class="h2">Добавить новый продукт</h1>

    <form action="/products" method="post">

        @csrf

        @include('partials.errors')

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputEmail4">Название</label>
                <input type="text" class="form-control" id="inputEmail4" name="name" value="{{old('name')}}">
            </div>

            <div class="form-group col-md-4">
                <label for="inputState">Категория</label>
                <select name="category_id" id="inputState" class="form-control">
                    <option selected>Выбери...</option>
                    @foreach($categories as $category)

                        @if(old('category_id') == $category->id)
                            <option value="{{$category->id}}" selected>{{$category->name}}</option>
                        @else
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endif

                    @endforeach
                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="inputEmail4">Залог</label>
                <input type="number" min="0" class="form-control" id="inputEmail4" name="deposit" value="{{old('deposit')}}">
            </div>

            <div class="form-group col-md-4">
                <label for="inputEmail4">Тариф 1 мес</label>
                @if(old('m1_tarif') > 0)
                    <input type="number" min="0" class="form-control" id="inputEmail4" name="m1_tarif" value="{{old('m1_tarif')}}">
                @else
                    <input type="number" min="0" class="form-control" id="inputEmail4" name="m1_tarif" value="0">
                @endif
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Тариф 2 мес</label>
                @if(old('m2_tarif') > 0)
                    <input type="number" min="0" class="form-control" id="inputEmail4" name="m2_tarif" value="{{old('m2_tarif')}}">
                @else
                    <input type="number" min="0" class="form-control" id="inputEmail4" name="m2_tarif" value="0">
                @endif
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Тариф 3 мес</label>
                @if(old('m3_tarif') > 0)
                    <input type="number" min="0" class="form-control" id="inputEmail4" name="m3_tarif" value="{{old('m3_tarif')}}">
                @else
                    <input type="number" min="0" class="form-control" id="inputEmail4" name="m3_tarif" value="0">
                @endif
            </div>

            <div class="form-group col-md-4">
                <label for="inputEmail4">Тариф 1 нед</label>
                @if(old('w1_tarif') > 0)
                    <input type="number" min="0" class="form-control" id="inputEmail4" name="w1_tarif" value="{{old('w1_tarif')}}">
                @else
                    <input type="number" min="0" class="form-control" id="inputEmail4" name="w1_tarif" value="0">
                @endif            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Тариф 2 нед</label>
                @if(old('w2_tarif') > 0)
                    <input type="number" min="0" class="form-control" id="inputEmail4" name="w2_tarif" value="{{old('w2_tarif')}}">
                @else
                    <input type="number" min="0" class="form-control" id="inputEmail4" name="w2_tarif" value="0">
                @endif            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Тариф 3 нед</label>
                @if(old('w3_tarif') > 0)
                    <input type="number" min="0" class="form-control" id="inputEmail4" name="w3_tarif" value="{{old('w3_tarif')}}">
                @else
                    <input type="number" min="0" class="form-control" id="inputEmail4" name="w3_tarif" value="0">
                @endif            </div>

        </div>

        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
@endsection
