@extends('layout.base')

@section('search')
    <form class="search_form" action="/search-client" method="get">
        <input class="form-control form-control-dark w-100" type="text" placeholder="Поиск клиента" aria-label="Search" name="query">
    </form>
@endsection

@section('content')
    <h3>Продление заказа на {{$order->product_name}}, клиента {{$order->client->surname}} {{$order->client->name}}</h3>

    <form action="/periods" method="post">

        @csrf

        @include('partials.errors')

        <input type="hidden" name="order_id" value="{{$order->id}}">
        <input type="hidden" name="product_id" value="{{$order->product_id}}">
        <input type="hidden" name="client_id" value="{{$order->client->id}}">

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputEmail4">Прокат с</label>
                <input type="date" class="form-control" id="inputEmail4" name="date_start" value="{{$currentPeriod->date_finish}}" readonly>
            </div>
        </div>


        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">На срок</label>
                <select type="number" min="0" class="form-control" id="inputEmail4" name="period">
                    @if($product->m3_tarif > 0)
                        @if($currentPeriod->period == '3months')
                            <option value="3months" selected>3 мес ({{$product->m3_tarif}} грн.)</option>
                        @else
                            <option value="3months">3 мес ({{$product->m3_tarif}} грн.)</option>
                        @endif
                    @endif
                    @if($product->m2_tarif > 0)
                        @if($currentPeriod->period == '2months')
                            <option value="2months" selected>2 мес ({{$product->m2_tarif}} грн.)</option>
                        @else
                            <option value="2months">2 мес ({{$product->m2_tarif}} грн.)</option>
                        @endif
                    @endif
                    @if($product->m1_tarif > 0)
                        @if($currentPeriod->period == '1month')
                            <option value="1month" selected>1 мес ({{$product->m1_tarif}} грн.)</option>
                        @else
                            <option value="1month">1 мес ({{$product->m1_tarif}} грн.)</option>
                        @endif
                    @endif
                    @if($product->w3_tarif > 0)
                        @if($currentPeriod->period == '3weeks')
                            <option value="3weeks" selected>3 нед ({{$product->w3_tarif}} грн.)</option>
                        @else
                            <option value="3weeks">3 нед ({{$product->w3_tarif}} грн.)</option>
                        @endif
                    @endif
                    @if($product->w2_tarif > 0)
                        @if($currentPeriod->period == '2weeks')
                            <option value="2weeks" selected>2 нед ({{$product->w2_tarif}} грн.)</option>
                        @else
                            <option value="2weeks">2 нед ({{$product->w2_tarif}} грн.)</option>
                        @endif
                    @endif
                    @if($product->w1_tarif > 0)
                        @if($currentPeriod->period == '1week')
                            <option value="1week" selected>1 нед ({{$product->w1_tarif}} грн.)</option>
                        @else
                            <option value="1week">1 нед ({{$product->w1_tarif}} грн.)</option>
                        @endif
                    @endif
                </select>
            </div>

            <div class="form-group col-md-6">
                <label for="inputEmail4">Cкидка</label>
                <input type="number" class="form-control" id="inputEmail4" name="discount" placeholder="Скидка в грн." value="{{$currentPeriod->discount}}">
            </div>

        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Индивидуальный срок</label>
                <input type="number" class="form-control" id="inputEmail4" name="new_period" placeholder="кол-во суток">
            </div>
            <div class="form-group col-md-6">
                <label for="inputEmail4">Тариф за сутки с учетом скидки</label>
                <input type="number" class="form-control" id="inputEmail4" name="tarif" value="{{$tarifPerDay}}">
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Продлить</button>
    </form>

@endsection
