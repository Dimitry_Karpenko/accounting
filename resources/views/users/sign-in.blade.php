@extends('layout.auth-base')

@section('content')
    <body class="text-center">
    <form class="form-signin" action="/sign-in" method="post">

        @csrf

        <img class="mb-4" src="https://xn--80atldfp.xn--j1amh/wp-content/uploads/2017/05/cropped-pokat.ukr-mainlogo-1.png" alt="" width="100%">
        <h1 class="h3 mb-3 font-weight-normal">Вход</h1>

        @include('partials.errors')

        @if(isset($noUser))
            <div class="alert alert-danger" role="alert">
                {{$noUser}}
            </div>
        @endif

        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Вход</button>
        <a href="/sign-up">Зарегистрироваться</a>
        <p class="mt-5 mb-3 text-muted">&copy; 2017-2020</p>
    </form>
    </body>
    </html>
@endsection

