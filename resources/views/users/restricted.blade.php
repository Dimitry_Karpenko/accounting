@extends('layout.auth-base')

@section('content')
        <body class="text-center">
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Внимание!</h4>
            <p>Уважаемый пользователь - {{Auth::user()->name}}. <br>Доступ запрещен</p>
            <hr>
            <p class="mb-0">Обратитесь к администратору. </p>
            <p class="mb-0"><a href="/">На главную</a></p>
        </div>
    </body>
    </html>
@endsection

