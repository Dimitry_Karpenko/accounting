@extends('layout.auth-base')

@section('content')
    <body class="text-center">
    <form class="form-signin" action="/sign-up" method="post">

        <img class="mb-4" src="https://xn--80atldfp.xn--j1amh/wp-content/uploads/2017/05/cropped-pokat.ukr-mainlogo-1.png" alt="" width="100%">
        <h1 class="h3 mb-3 font-weight-normal">Регистрация</h1>

        @csrf

        @include('partials.errors')

        <label for="inputEmail" class="sr-only">Имя</label>
        <input type="name" id="inputEmail" class="form-control" placeholder="Имя" name="name" value="{{old('name')}}">
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email" name="email" value="{{old('email')}}">
        <label for="inputPassword" class="sr-only">Пароль</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Пароль" name="password">
        <label for="inputPassword" class="sr-only">Подтвердите пароль</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Подтвердите пароль" name="password_confirmation">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Зарегистрироваться</button>

        <p class="mt-5 mb-3 text-muted">&copy; 2017-2020</p>
    </form>
    </body>
    </html>
@endsection
