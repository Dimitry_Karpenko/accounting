<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetDefaultForTarifs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table){
            $table->smallInteger('m3_tarif')->default(0)->change();
            $table->smallInteger('m2_tarif')->default(0)->change();
            $table->smallInteger('m1_tarif')->default(0)->change();
            $table->smallInteger('w3_tarif')->default(0)->change();
            $table->smallInteger('w2_tarif')->default(0)->change();
            $table->smallInteger('w1_tarif')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->smallInteger('m3_tarif')->default(null)->change();
        $table->smallInteger('m2_tarif')->default(null)->change();
        $table->smallInteger('m1_tarif')->default(null)->change();
        $table->smallInteger('w3_tarif')->default(null)->change();
        $table->smallInteger('w2_tarif')->default(null)->change();
        $table->smallInteger('w1_tarif')->default(null)->change();
    }
}
