<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTypeOfColumnProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table){
            $table->smallInteger('deposit')->change();
            $table->smallInteger('3m_tarif')->change();
            $table->smallInteger('2m_tarif')->change();
            $table->smallInteger('1m_tarif')->change();
            $table->smallInteger('3w_tarif')->change();
            $table->smallInteger('2w_tarif')->change();
            $table->smallInteger('1w_tarif')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->tinyInteger('deposit')->change();
        $table->tinyInteger('3m_tarif')->change();
        $table->tinyInteger('2m_tarif')->change();
        $table->tinyInteger('1m_tarif')->change();
        $table->tinyInteger('3w_tarif')->change();
        $table->tinyInteger('2w_tarif')->change();
        $table->tinyInteger('1w_tarif')->change();
    }
}
