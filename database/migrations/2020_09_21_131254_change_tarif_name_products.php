<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTarifNameProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table){
            $table->renameColumn('3m_tarif', 'm3_tarif');
            $table->renameColumn('2m_tarif', 'm2_tarif');
            $table->renameColumn('1m_tarif', 'm1_tarif');
            $table->renameColumn('3w_tarif', 'w3_tarif');
            $table->renameColumn('2w_tarif', 'w2_tarif');
            $table->renameColumn('1w_tarif', 'w1_tarif');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->renameColumn('m3_tarif', '3m_tarif');
        $table->renameColumn('m2_tarif', '2m_tarif');
        $table->renameColumn('m1_tarif', '1m_tarif');
        $table->renameColumn('w3_tarif', '3w_tarif');
        $table->renameColumn('w2_tarif', '2w_tarif');
        $table->renameColumn('w1_tarif', '1w_tarif');
    }
}
