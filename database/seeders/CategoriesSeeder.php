<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Без категории',
                'user_id' => 1,
            ],
            [
                'name' => 'Коляска',
                'user_id' => 1,
            ],
            [
                'name' => 'Укач.центр',
                'user_id' => 1,

            ],
            [
                'name' => 'Манеж',
                'user_id' => 1,

            ],[
                'name' => 'Шезлонг',
                'user_id' => 1,

            ],
            [
                'name' => 'Весы',
                'user_id' => 1,

            ],
        ]);
    }
}
