<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            [
                'name' => 'Дмитрий',
                'otchestvo' => 'Вячеславович',
                'surname' => 'Карпенко',
                'tel' => '0949475080',
                'viber' => '0949475080',
                'address' => 'ул. Бабушкина, 9а',
                'user_id' => 1,

            ],
            [
                'name' => 'Василий',
                'otchestvo' => 'Дмитриевич',
                'surname' => 'Петров',
                'tel' => '0949475080',
                'viber' => '0949475080',
                'address' => 'ул. Королева 100, кв 12',
                'user_id' => 1,
            ],
            [
                'name' => 'Светлана',
                'otchestvo' => 'Игоревна',
                'surname' => 'Ильченко ',
                'tel' => '0949475080',
                'viber' => '0949475080',
                'address' => 'Ильичевск, ул. 1 Мая, 12, кв. 22',
                'user_id' => 1,
            ],
            [
                'name' => 'Стас',
                'otchestvo' => 'Олегович',
                'surname' => 'Ветров',
                'tel' => '0949475080',
                'viber' => '0949475080',
                'address' => 'ул. Балковская 55, кв 1',
                'user_id' => 1,
            ],
        ]);
    }
}
