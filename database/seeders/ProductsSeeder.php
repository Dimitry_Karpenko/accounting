<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'name' => 'Gamma',
                'category_id' => 6,
                'deposit' => 350,
                'm2_tarif' => 450,
                'm3_tarif' => 320,
                'm1_tarif' => 170,
                'w3_tarif' => 0,
                'w2_tarif' => 0,
                'w1_tarif' => 0,
                'user_id' => 1,

            ],[
                'name' => 'Momert',
                'category_id' => 6,
                'deposit' => 350,
                'm2_tarif' => 450,
                'm3_tarif' => 320,
                'm1_tarif' => 170,
                'w3_tarif' => 0,
                'w2_tarif' => 0,
                'w1_tarif' => 0,
                'user_id' => 1,

            ],
            [
                'name' => 'Mamaroo 4.0',
                'category_id' => 3,
                'deposit' => 1000,
                'm2_tarif' => 0,
                'm3_tarif' => 0,
                'm1_tarif' => 800,
                'w3_tarif' => 750,
                'w2_tarif' => 450,
                'w1_tarif' => 400,
                'user_id' => 1,

            ],
            [
                'name' => 'Graco Move With Me',
                'category_id' => 3,
                'deposit' => 1000,
                'm2_tarif' => 0,
                'm3_tarif' => 0,
                'm1_tarif' => 800,
                'w3_tarif' => 750,
                'w2_tarif' => 450,
                'w1_tarif' => 400,
                'user_id' => 1,

            ],
            [
                'name' => 'Carrello Pilot',
                'category_id' => 2,
                'deposit' => 1000,
                'm2_tarif' => 0,
                'm3_tarif' => 0,
                'm1_tarif' => 800,
                'w3_tarif' => 750,
                'w2_tarif' => 450,
                'w1_tarif' => 400,
                'user_id' => 1,

            ],
            [
                'name' => 'YoYo+',
                'category_id' => 2,
                'deposit' => 1000,
                'm2_tarif' => 0,
                'm3_tarif' => 0,
                'm1_tarif' => 800,
                'w3_tarif' => 750,
                'w2_tarif' => 450,
                'w1_tarif' => 400,
                'user_id' => 1,

            ],
            [
                'name' => 'Carrelo  Grand',
                'category_id' => 4,
                'deposit' => 1000,
                'm2_tarif' => 0,
                'm3_tarif' => 0,
                'm1_tarif' => 800,
                'w3_tarif' => 750,
                'w2_tarif' => 450,
                'w1_tarif' => 400,
                'user_id' => 1,

            ],
            [
                'name' => 'Nuna',
                'category_id' => 4,
                'deposit' => 1000,
                'm2_tarif' => 0,
                'm3_tarif' => 0,
                'm1_tarif' => 800,
                'w3_tarif' => 750,
                'w2_tarif' => 450,
                'w1_tarif' => 400,
                'user_id' => 1,

            ],
            [
                'name' => 'Disney',
                'category_id' => 5,
                'deposit' => 1000,
                'm2_tarif' => 0,
                'm3_tarif' => 0,
                'm1_tarif' => 800,
                'w3_tarif' => 750,
                'w2_tarif' => 450,
                'w1_tarif' => 400,
                'user_id' => 1,

            ],
            [
                'name' => 'Bright Starts',
                'category_id' => 5,
                'deposit' => 1000,
                'm2_tarif' => 0,
                'm3_tarif' => 0,
                'm1_tarif' => 800,
                'w3_tarif' => 750,
                'w2_tarif' => 450,
                'w1_tarif' => 400,
                'user_id' => 1,

            ],
        ]);
    }
}
