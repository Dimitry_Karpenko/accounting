<?php

namespace App\Http\Controllers;

use App\Http\Requests\Children\StoreChildRequest;
use App\Http\Requests\Children\UpdateChildRequest;
use App\Models\Child;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChildrenController extends Controller
{
    public function create(Client $client)
    {
        return view('children.create', compact('client'));
    }

    public function store(StoreChildRequest $request)
    {

        Child::create($request->all(['name', 'sex', 'birthday', 'client_id']) + ['user_id' => Auth::id()]);

        return redirect('/relatives/create/'.$request->post('client_id'));


    }

    public function destroy(Child $child, Request $request)
    {
        $child->delete();

        return redirect('/clients/' . $request->post('client_id'));

    }

    public function edit(Child $child)
    {
        return view('children.edit', compact('child'));
    }

    public function update (UpdateChildRequest $request, Child $child)
    {
        $child->update($request->all());

        return redirect('/clients/' . $request->post('client_id'));
    }

    public function delete(Child $child)
    {
        return view('children.delete', compact('child'));
    }
}
