<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\UsersRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function create()
    {
        return view('users.sign-up');
    }

    public function store(UsersRequest $request)
    {
        $user = User::create($request->all(['name', 'email']) + ['password' => bcrypt($request->post('password'))]);

        Auth::login($user);

        return redirect()->route('clients');
    }
}
