<?php

namespace App\Http\Controllers;

use App\Http\Requests\Relatives\RelativesRequest;
use App\Models\Client;
use App\Models\Relative;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RelativesController extends Controller
{
    public function create(Client $client)
    {
        return view('relatives.create', compact('client'));
    }

    public function store(RelativesRequest $request)
    {
        Relative::create($request->all(['name', 'relationship', 'tel', 'client_id']) + ['user_id' => Auth::id()]);

        return redirect('/clients/' . $request->post('client_id'));
    }

    public function edit(Relative $relative)
    {
        return view('relatives.edit', compact('relative'));
    }

    public function update(RelativesRequest $request, Relative $relative)
    {
        $relative->update($request->all());

        return redirect('/clients/' . $relative->client_id);
    }

    public function delete(Relative $relative)
    {
        return view('relatives.delete', compact('relative'));
    }

    public function destroy(Relative $relative)
    {
        $relative->delete();

        return redirect('/clients/' . $relative->client->id);
    }
}
