<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Order;
use App\Models\Period;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PeriodsController extends Controller
{
    public function create(Order $order)
    {
        $currentPeriod = $order->currentPeriod();
        $product = $order->product;
        $tarifPerDay = round($currentPeriod->tarifPerDay(), 0) ;

        return view('periods.create', compact('currentPeriod', 'order', 'product', 'tarifPerDay'));
    }

    public function store(Request $request)
    {
        if ($request->post('new_period')){
            $period = $request->post('new_period') . 'days';
            $tarif = $request->post('tarif') * $request->post('new_period') ;
            $discount = 0;
        }else{
            $period = $request->post('period');
            $tarif = Product::tarifByPeriod($request->post('product_id'), $request->post('period'));
            $discount = $request->post('discount');
        }

        $sum = $tarif - $discount;

        Period::create($request->all(['order_id', 'date_start']) + ['user_id' => Auth::id(), 'period' => $period, 'sum' => $sum, 'tarif' => $tarif,  'discount' => $discount,  'date_finish' => date('Y-m-d', strtotime($request->post('date_start') . '+' . $period))]);

        return redirect('/clients/' . $request->post('client_id'));

    }
}
