<?php

namespace App\Http\Controllers;

use App\Http\Requests\Products\ProductsRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $categories = Category::all();

        return view('products.index', compact('products', 'categories'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('products.create', compact('categories'));
    }

    public function store(ProductsRequest $request, Product $product)
    {
        $product->create($request->all(['name', 'category_id', 'deposit', 'm3_tarif', 'm2_tarif', 'm1_tarif', 'w3_tarif', 'w2_tarif', 'w1_tarif']) + ['user_id' => Auth::id()]);

        return redirect('/products');
    }

    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('products.edit', compact('product', 'categories'));
    }

    public function update(ProductsRequest $request, Product $product)
    {
        $product->update($request->all());

        return redirect('/products/' . $product->id);
    }

    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    public function showByCategory(Category $category)
    {
        $products = $category->products;

        $categories = Category::all();

        return view('products.show-by-category', compact('categories', 'products', 'category'));
    }
}
