<?php

namespace App\Http\Controllers;

use App\Http\Requests\Clients\StoreClientRequest;
use App\Http\Requests\Clients\UpdateClientRequest;
use App\Models\Client;
use Illuminate\Support\Facades\Auth;

//use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function index()
    {
        $clients = Client::all()->sortByDesc('id');
        return view('clients.index', compact('clients'));
    }

    public function create()
    {
        return view('clients.create');
    }

    public function store(StoreClientRequest $request)
    {

        $client = Client::create($request->all(['name', 'otchestvo', 'surname', 'tel', 'viber', 'address']) + ['user_id' => Auth::id()]);

        $id = $client->id;

        return redirect('/children/create/'.$id);
    }

    public function show(Client $client)
    {
        return view('clients.show', compact('client'));
    }

    public function delete(Client $client)
    {
        return view('clients.delete', compact('client'));
    }

    public function destroy(Client $client)
    {
        $client->delete();

        return redirect('/clients/');
    }

    public function edit(Client $client)
    {
        return view('clients.edit', compact('client'));
    }

    public function update(UpdateClientRequest $request, Client $client)
    {
        $client->update($request->all());

        return redirect('/clients/'.$client->id);
    }
}
