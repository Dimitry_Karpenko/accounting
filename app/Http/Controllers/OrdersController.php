<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Client;
use App\Models\Order;
use App\Models\Period;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class OrdersController extends Controller
{
    public function index()
    {

        $orders = Order::all()->sortBy(function ($order){
            return strtotime($order->getFinishDate());
        });

        return view('orders.index',compact('orders'));
    }

    public function show(Order $order)
    {
        return view('orders.show', compact('order'));
    }

    public function selectCategory(Client $client)
    {
        $categories = Category::all();
        return view('orders.categories', compact('client', 'categories'));
    }

    public function selectProduct(Client $client, Category $category)
    {
        $products = $category->products;

        return view('orders.products', compact('client', 'products'));
    }

    public function create(Client $client, Product $product)
    {
        return view('orders.create', compact('client', 'product'));
    }

    public function store(Request $request)
    {
        $tarif = Product::tarifByPeriod($request->post('product_id'), $request->post('period'));
        $sum = $tarif - $request->post('discount');

        $order = Order::create($request->all(['client_id', 'product_id', 'product_name', 'product_type', 'deposit', 'comment']) + ['user_id' => Auth::id(), 'active' => 1]);

        $period = Period::create($request->all(['period', 'discount']) + ['user_id' => Auth::id(), 'sum' => $sum, 'tarif' => $tarif, 'order_id' => $order->id,'date_start' => date('Y-m-d', strtotime($request->post('date_start'))), 'date_finish' => date('Y-m-d', strtotime($request->post('date_start'). '+' . $request->post('period')))]);

        return redirect('/clients/' . $request->post('client_id'));

    }
    public function finishedTerm()
    {
        $orders = Order::all();

        return view('orders.finished-term',compact('orders'));
    }

    public function delete(Order $order){

        return view('orders.delete', compact('order'));
    }

    public function destroy(Order $order)
    {
        $order->delete();

        return redirect('/clients/' . $order->client->id);
    }

    public function edit(Order $order)
    {
        return view('orders.edit', compact('order'));
    }

    public function update(Order $order, Request $request)
    {
        $order->update($request->all());

        return redirect('/clients/' . $order->client->id);
    }

}
