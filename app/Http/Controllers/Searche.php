<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Product;
use Illuminate\Http\Request;

class Searche extends Controller
{
    public function showClient(Request $request)
    {
        $query = $request->get('query');
        $clients = Client::query()
            ->where('surname' , 'LIKE', "%$query%")
            ->orWhere('tel' , 'LIKE', "%$query%")
            ->orWhere('viber' , 'LIKE', "%$query%")
            ->get();

        return view('clients.index', compact('clients'));
    }

    public function showProduct(Request $request)
    {
        $query = $request->get('query');
        $products = Product::query()
            ->where('name' , 'LIKE', "%$query%")
            ->get();

        return view('products.index', compact('products'));
    }
}
