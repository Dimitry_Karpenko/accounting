<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\SignInUserRequest;
use App\Http\Requests\Users\UsersRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function create()
    {
        return view('users.sign-in');
    }

    public function store(SignInUserRequest $request)
    {
        $noUser = "Пользователя не существует";

        if (Auth::attempt($request->all(['email', 'password']))){

            return redirect()->route('clients');
        }


        return view('users.sign-in', compact('noUser'));

    }

    public function destroy()
    {
        if (Auth::check()){
            Auth::logout();
        }

        return redirect()->route('sign-in');
    }

    public function restricted(){
        return view('users.restricted');
    }

}
