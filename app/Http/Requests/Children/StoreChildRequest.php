<?php

namespace App\Http\Requests\Children;

use Illuminate\Foundation\Http\FormRequest;

class StoreChildRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'client_id' => 'required',
            'sex' => 'min:1',
            'birthday' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Заполните поле Имя',
            'sex.min' => 'Выберите пол',
            'birthday.required' => 'Выберите дату рождения',
        ];
    }
}
