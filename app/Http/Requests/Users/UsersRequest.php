<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|confirmed'

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Заполните поле имя',
            'email.required' => 'Заполните поле почта',
            'password.required' => 'Заполните поле пароль',
            'name.min' => 'Имя должно быть больше 2 симоволов',
            'email.unique' => 'Такой адрес почты уже есть',
            'password.min' => 'Пароль должен быть не меньше 6 символов',
            'password.confirmed' => 'Пароль не совпал',
        ];
    }
}
