<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class ProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'category_id' => 'integer',
            'deposit' => 'required',
            'm1_tarif' => 'required',
            'm2_tarif' => 'required',
            'm3_tarif' => 'required',
            'w1_tarif' => 'required',
            'w2_tarif' => 'required',
            'w3_tarif' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Заполните поле название',
            'name.min' => 'Поле название должно быть больше 2 символов',
            'category_id.integer' => 'Выберите категорию',
            'deposit.required' => 'Заполните поле залог',
            'm1_tarif.required' => 'Заполните поле тариф 1мес',
            'm2_tarif.required' => 'Заполните поле тариф 2мес',
            'm3_tarif.required' => 'Заполните поле тариф 3мес',
            'w1_tarif.required' => 'Заполните поле тариф 1нед',
            'w2_tarif.required' => 'Заполните поле тариф 2нед',
            'w3_tarif.required' => 'Заполните поле тариф 3нед',
        ];
    }
}
