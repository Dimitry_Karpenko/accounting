<?php

namespace App\Http\Requests\Clients;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'surname' => 'required|min:3',
            'tel' => 'required|regex:/^0\d{9}/',
            'viber' => 'required|regex:/^\+380\d{9}/',
            'address' => 'required|min:5'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Поля имя должно быть заполнено',
            'surname.required' => 'Поля фамилия должно быть заполнено',
            'tel.required' => 'Поля телефон должно быть заполнено',
            'viber.required' => 'Поля вайбер должно быть заполнено',
            'address.required' => 'Поля адрес должно быть заполнено',
            'name.min' => 'Имя должно быть больше 3 символов',
            'surname.min' => 'Фамилия должна быть больше 3 символов',
            'address.min' => 'Адрес должен быть больше 3 символов',
            'tel.regex' => 'Тел формат 0ХХХХХХХХХ (всего 10 символов без пробелов)',
            'viber.regex' => 'Вайбер формат +380ХХХХХХХХХ (всего 13 символов без пробелов)',
        ];
    }
}
