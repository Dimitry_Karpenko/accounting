<?php

namespace App\Http\Requests\Relatives;

use Illuminate\Foundation\Http\FormRequest;

class RelativesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'relationship' => 'required|min:3',
            'tel' =>'required|regex:/^0\d{9}/'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Заполните поле имя',
            'name.min' => 'Имя должно быть больше 2 знаков',
            'relationship.required' => 'Заполните поле род.связь',
            'relationship.min' => 'род.связь должно быть больше 2 знаков',
            'tel.required' => 'Заполните поле телефон',
            'tel.regex' => 'Тел формат 0ХХХХХХХХХ (всего 10 символов без пробелов)',
        ];
    }
}
