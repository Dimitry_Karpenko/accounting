<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Relative extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'relationship', 'tel', 'client_id', 'user_id'];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

}
