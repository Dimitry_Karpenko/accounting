<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    use HasFactory;

    public $fillable = ['name', 'sex', 'birthday', 'client_id', 'user_id'];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function getBirthday()
    {
        return date('d.m.y', strtotime($this->birthday));
    }

    public function getAge()
    {

        $diff = strtotime(date('Ymd')) - strtotime($this->birthday);
        $days = floor($diff / 86400);
        $years = floor($days / 365);
        $months = floor($days % 365 / 30);

        $result = '';

        if ($years == 1){
            $result .= $years . ' год ';
        }elseif ($years > 0 && $years < 5){
            $result .= $years . ' годa ';
        }elseif ($years > 4){
            $result .= $years . ' лет ';
        }

        if ($months == 1){
            $result .= $months . ' месяц';
        }elseif ($months >1 && $months < 5){
            $result .= $months . ' месяцa ';
        }elseif ($months > 4){
            $result .= $months . ' месяцев ';
        }



        return $result;
    }
}
