<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'category_id', 'deposit', 'm3_tarif', 'm2_tarif', 'm1_tarif', 'w3_tarif', 'w2_tarif', 'w1_tarif', 'user_id'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public static function tarifByPeriod($product_id, $period)
    {
        $find = ['3months', '2months', '1month', '3weeks', '2weeks', '1week', ];
        $replace = ['m3_tarif', 'm2_tarif', 'm1_tarif', 'w3_tarif', 'w2_tarif', 'w1_tarif', ];

        $tarifName = str_replace($find, $replace, $period);

        $product = self::find($product_id);
        $tarif = $product->$tarifName;

        return $tarif;
    }

}
