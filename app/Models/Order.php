<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['client_id', 'product_id', 'product_name', 'product_type', 'deposit', 'comment', 'active', 'user_id'];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function periods()
    {
        return $this->hasMany(Period::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function currentPeriod()
    {

        return $this->periods->last();
    }

    public function getFinishDate()
    {

        $finishDate = date('d-m-Y', strtotime($this->currentPeriod()->date_finish));

        return $finishDate;

    }


    public function finishedTerm($deadline = '')
    {
        $finished = false;

        if(strtotime($this->getFinishDate()) < strtotime(date('d-m-Y') . $deadline)){

            $finished = true;
        }

        return $finished;
    }

    public function currentPeriodInHuman()
    {
        $tarifName = '';

        if ($this->currentPeriod()->period){
            $find = ['3months', '2months', '1month', '3weeks', '2weeks', '1week', 'days'];
            $replace = ['3 мес.', '2 мес.', 'мес.', '3 нед.', '2 нед.', 'нед.', ' дн.'];

            $tarifName = str_replace($find, $replace, $this->currentPeriod()->period);
        }

        return $tarifName;
    }

    public function currentTarif()
    {
        return $this->currentPeriod()->tarif;
    }

    public function currentDiscount()
    {
        return $this->currentPeriod()->discount;
    }

    public function currentSum()
    {
        return $this->currentPeriod()->sum;
    }
}
