<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    use HasFactory;

    protected $fillable = ['order_id', 'period', 'tarif', 'discount', 'sum', 'date_start', 'date_finish', 'user_id'];

    public function order()
    {
        $this->belongsTo(Order::class);
    }

    public function tarifPerDay()
    {
        $find = ['3months', '2months', '1month', '3weeks', '2weeks', '1week', 'days'];
        $replace = [90, 60, 30, 21, 14, 7, ''];

        $period = str_replace($find, $replace, $this->period);

        $tarifPerDay = $this->tarif / $period;

        return $tarifPerDay;
    }

}
