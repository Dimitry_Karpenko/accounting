<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'otchestvo', 'surname', 'tel', 'viber', 'address', 'user_id'];

    public function children()
    {
        return $this->hasMany(Child::class);
    }

    public function relatives()
    {
        return $this->hasMany(Relative::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

}
