<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientsController;
use App\Http\Controllers\ChildrenController;
use App\Http\Controllers\Searche;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\RelativesController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PeriodsController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function (){
    return redirect()->route('orders');
});

Route::get('/sign-in', [LoginController::class, 'create'])->name('sign-in');
Route::post('/sign-in', [LoginController::class, 'store']);

Route::middleware('guest')->group(function(){
    Route::get('/sign-up', [UserController::class, 'create'])->name('sign-up');
    Route::post('/sign-up', [UserController::class, 'store']);

});

Route::delete('/logout', [LoginController::class,'destroy']);

Route::get('/restricted', [LoginController::class,'restricted'])->middleware('auth');

Route::middleware('admin')->group(function(){
    Route::get('/clients', [ClientsController::class, 'index'])->name('clients');
    Route::get('/clients/create/', [ClientsController::class, 'create']);
    Route::post('/clients/', [ClientsController::class, 'store']);
    Route::get('/clients/{client}', [ClientsController::class, 'show']);
    Route::get('/clients/{client}/delete', [ClientsController::class, 'delete']);
    Route::delete('/clients/{client}', [ClientsController::class, 'destroy']);
    Route::get('/clients/{client}/edit', [ClientsController::class, 'edit']);
    Route::put('/clients/{client}', [ClientsController::class, 'update']);

    Route::get('/orders', [OrdersController::class, 'index'])->name('orders');
    Route::get('/orders/{order}', [OrdersController::class, 'show']);
    Route::get('/orders/{order}/delete', [OrdersController::class, 'delete']);
    Route::delete('/orders/{order}', [OrdersController::class, 'destroy']);
    Route::get('/orders/{order}/edit', [OrdersController::class, 'edit']);
    Route::put('/orders/{order}', [OrdersController::class, 'update']);

    Route::get('/clients/{client}/category', [OrdersController::class, 'selectCategory']);
    Route::get('/clients/{client}/category/{category}', [OrdersController::class, 'selectProduct']);
    Route::get('/clients/{client}/product/{product}', [OrdersController::class, 'create']);
    Route::post('/orders', [OrdersController::class, 'store']);
    Route::get('/orders/finished-term', [OrdersController::class, 'finishedTerm']);

    Route::get('/periods/create/{order}', [PeriodsController::class, 'create']);
    Route::post('/periods', [PeriodsController::class, 'store']);


    Route::get('/children/create/{client}', [ChildrenController::class, 'create']);
    Route::post('/children', [ChildrenController::class, 'store']);
    Route::get('/children/{child}/delete', [ChildrenController::class, 'delete']);
    Route::delete('/children/{child}', [ChildrenController::class, 'destroy']);
    Route::get('/children/{child}/edit', [ChildrenController::class, 'edit']);
    Route::put('/children/{child}', [ChildrenController::class, 'update']);

    Route::get('/relatives/create/{client}', [RelativesController::class, 'create']);
    Route::post('/relatives/', [RelativesController::class, 'store']);
    Route::get('/relatives/{relative}/edit', [RelativesController::class, 'edit']);
    Route::put('/relatives/{relative}', [RelativesController::class, 'update']);
    Route::get('/relatives/{relative}/delete', [RelativesController::class, 'delete']);
    Route::delete('/relatives/{relative}', [RelativesController::class, 'destroy']);

    Route::get('/search-client', [Searche::class, 'showClient']);
    Route::get('/search-product', [Searche::class, 'showProduct']);

    Route::resources([
        'categories' => CategoriesController::class,
        'products' => ProductsController::class,
    ]);

    Route::get('/products/category/{category}', [ProductsController::class, 'showByCategory']);

    Route::get('/categories/{category}/delete', [CategoriesController::class, 'delete']);


});




